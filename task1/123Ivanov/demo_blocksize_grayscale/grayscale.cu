#include <common/common_functions.cu>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>

// This function is executed on DEVICE and can be called both from HOST and DEVICE.
__global__ void _grayscale(const glm::u8vec4* src_image, glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea) {
	glm::ivec2 workPixel(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);

	int srcOffset = count_img_offset(workPixel, srcWorkArea);
	int dstOffset = count_img_offset(workPixel, dstWorkArea);

	if (srcOffset < 0 || dstOffset < 0)
		return;

	const glm::vec4 lightness_factor(0.3,0.59,0.11,0.0); // in sum they give 1.0;
	glm::vec4 src_color = glm::vec4(src_image[srcOffset]); // range [0..255]^4
	float lightness = glm::dot(src_color, lightness_factor); // [0..255]

	glm::vec4 dst_color(lightness, lightness, lightness, src_color.a); // preserve opacity.
	dst_image[dstOffset] = glm::u8vec4(dst_color);
}

// This is wrapper function for calling DEVICE _grayscale function from HOST.
void grayscale(const glm::u8vec4* src_image, glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim) {
	assert(glm::all(glm::equal(srcWorkArea.workDim, dstWorkArea.workDim)));

	dim3 blkDim(blockDim.x, blockDim.y, 1);
	glm::uvec2 gridDim = glm::uvec2(glm::ivec2(srcWorkArea.workDim + blockDim) - 1) / blockDim;
	dim3 grdDim = { gridDim.x, gridDim.y, 1 };

	_grayscale<<<grdDim, blkDim>>>(src_image, dst_image, srcWorkArea, dstWorkArea);
	checkCudaErrors(cudaGetLastError());
}
