#pragma once

#include <iostream>
#include <cuda_runtime_api.h>


#define checkCudaErrors(val) check( (val), #val, __FILE__, __LINE__)

template<typename T>
void check(T err, const char* const func, const char* const file, const int line) {
	if (err != cudaSuccess) {
		std::cerr << "CUDA error at: " << file << ":" << line << " " << func << std::endl;
		std::cerr << cudaGetErrorName(err) << ": " << cudaGetErrorString(err) << std::endl;
		throw "Cuda error";
	}
}
